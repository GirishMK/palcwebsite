$(document).ready(function () {
    // window.setTimeout(function(){
    //    
    // },3000)
    //  all body level events
    $("body").click(function () {
        // click if collapse nav-bar is showing
        if ($(".collapse.navbar-collapse.show").length) {
            $(".collapse.navbar-collapse.show").collapse("hide");
        }
    })
    // closing the navbar if any option clicked
    $(".collapse.navbar-collapse .nav-list").click(function () {
        $(".collapse.navbar-collapse.show").collapse("hide");
    })

    // redirection from the navbar options scrolling smoothly
    $(".link_to_section").click(function (event) {
        let to = "#section_home";
        switch (event.currentTarget.firstElementChild.id) {
            case "link_home":
                to = "#section_home";
                break;
            case "link_about_us":
                to = "#section_about_us";
                break;
            case "link_services":
                to = "#section_services";
                break;
            case "link_contact_us":
                to = "#section_contact_us";
                break;
        }
        $('html, body').animate({
            scrollTop: $(to).offset().top
        }, 800);
    })
    $("#facebook").mouseenter(function () {
        $("#fb1").css("display", "none")
        $("#fb2").css("display", "block");
    });
    $("#facebook").mouseleave(function () {
        $("#fb1").css("display", "block");
        $("#fb2").css("display", "none");
    });
    $("#linkedin").mouseenter(function () {
        $("#in1").css("display", "none")
        $("#in2").css("display", "block");
    });
    $("#linkedin").mouseleave(function () {
        $("#in1").css("display", "block");
        $("#in2").css("display", "none");
    });
    $("#youtube").mouseenter(function () {
        $("#youtube1").css("display", "none")
        $("#youtube2").css("display", "block");
    });
    $("#youtube").mouseleave(function () {
        $("#youtube1").css("display", "block");
        $("#youtube2").css("display", "none");
    });
    $("#twitter").mouseenter(function () {
        $("#twitter1").css("display", "none")
        $("#twitter2").css("display", "block");
    });
    $("#twitter").mouseleave(function () {
        $("#twitter1").css("display", "block");
        $("#twitter2").css("display", "none");
    });
    // document.getElementById("banner").autoplay = true; 
    // document.getElementById("banner").play();  

    //lazy load image
    // window.onload = function () {
    //     $($("img")).each(function (i, v) {
    //         if ($(v).attr("data-src")) {
    //             $(v).attr("src", $(v).attr("data-src"));
    //         }

    //     })
    // };
    $(".downloadbrochure").on("click", function (e) {
        if ($(e.currentTarget).hasClass("downloadbrochure")) {
            var topen = $(e.currentTarget).data("ln");
            if (topen) {
                window.open("http://falcasolutions.com/brochure/" + topen + ".pdf");
            }
        }
    });

    $(".flipper-container").on("click", function (e) {
        e.preventDefault();
        var target = $(e.currentTarget).find("#section_homee");
        if (!target.hasClass("flipperrorate")) {
            target.addClass("flipperrorate").animate({
                "visibility": "hidden"
            });
            $(".flipz").show().animate("ease-in", 1000);
        }
        else {
            target.removeClass("flipperrorate");
            $(".flipz").hide();
            target.css({
                "visibility": "visible"
            })
        }
    })

    $('#flipto').on("click", function (event) {
        event.preventDefault();

        var face = $(this).data("face");

        if (face == "bottom") {
            $(".cube").removeClass("flip-to-top").addClass("flip-to-bottom");
            $(this).data("face", "top").text("Flip: to top");
        } else {
            $(".cube").removeClass("flip-to-bottom").addClass("flip-to-top");
            $(this).data("face", "bottom").text("Flip: to bottom");
        }
    });
    var objAgent = navigator.userAgent;
    var objbrowserName = navigator.appName;
    var objOffsetName, objOffsetVersion, ix;
    if ((objOffsetVersion = objAgent.indexOf("MSIE")) != -1) {
        objbrowserName = "Microsoft Internet Explorer";
        objfullVersion = objAgent.substring(objOffsetVersion + 5);
    }
    // For other browser "name/version" is at the end of userAgent 
    else if ((objOffsetName = objAgent.lastIndexOf(' ') + 1) <
        (objOffsetVersion = objAgent.lastIndexOf('/'))) {
        objbrowserName = objAgent.substring(objOffsetName, objOffsetVersion);
        objfullVersion = objAgent.substring(objOffsetVersion + 1);
        if (objbrowserName.toLowerCase() == objbrowserName.toUpperCase()) {
            objbrowserName = navigator.appName;
        }
    }
    if (objbrowserName === "Netscape") {
        $(".iebrowse").css({ "display": "block" });
    } else {
        $(".otherbrowser").css({ "display": "block" });
    }






    var objAgent = navigator.userAgent;
    var objbrowserName = navigator.appName;
    var objOffsetName, objOffsetVersion, ix;
    if ((objOffsetVersion = objAgent.indexOf("MSIE")) != -1) {
        objbrowserName = "Microsoft Internet Explorer";
        objfullVersion = objAgent.substring(objOffsetVersion + 5);
    }
    // For other browser "name/version" is at the end of userAgent 
    else if ((objOffsetName = objAgent.lastIndexOf(' ') + 1) <
        (objOffsetVersion = objAgent.lastIndexOf('/'))) {
        objbrowserName = objAgent.substring(objOffsetName, objOffsetVersion);
        objfullVersion = objAgent.substring(objOffsetVersion + 1);
        if (objbrowserName.toLowerCase() == objbrowserName.toUpperCase()) {
            objbrowserName = navigator.appName;
        }
    }
    if (objbrowserName === "Netscape") {
        $(".iebrowse").css({ "display": "block" });
    } else {
        $(".otherbrowser").css({ "display": "block" });
    }


    // $('.contact-sent-btn').click(function (event) {
    //     
    //     var emailUser = $('.email').val();
    //     var phoneNumberUser = $('.phonenumber').val();
    //     var userInput = $('.textarea').val();
    //     //check for valid inputs  
    //     if (validEmail(emailUser) && validPhoneNumber(phoneNumberUser) && validName(userInput)) {
    //         //     var form = document.getElementById('gform');
    //         //     form.addEventListener("submit", handleFormSubmit, false);
    //         handleFormSubmit(event);
    //     }
    //     else {
    //         alert('Enter all valid details')
    //         //prevent form posting           
    //         event.preventDefault();
    //         //console.log('invlaid')
    //     }
    // })




    $('.contactus-btn').click(function (event) {
        debugger
        var userName = $("#contactsname").val();
        var organizationName = $("#contactusorganizationname").val();
        var role = $("#contactusrole").val();
        var weburl = $("#contactuswebsiteurl").val();
        var emailUser = $('#contactusemail').val();
        var phoneNumberUser = $('#contactusphone').val();
        var userInput = $('#contactustextarea').val();
        //check for valid inputs  
        if (validEmail(emailUser) && validPhoneNumber(phoneNumberUser) && validName(userInput)
            && validUserName(userName) && validOrganizationName(organizationName) && validRole(role)) {
            //     var form = document.getElementById('gform');
            //     form.addEventListener("submit", handleFormSubmit, false);
            handleFormSubmit(event, "cform");
        }
        else {
            alert('Enter all valid details')
            //prevent form posting           
            event.preventDefault();
            //console.log('invlaid')
        }
    })

    $('.salesinquiry-btn').click(function (event) {
        debugger
        var userName = $("#salesname").val();
        var organizationName = $("#salesorganizationname").val();
        var role = $("#salesrole").val();
        var weburl = $("#saleswebsiteurl").val();
        var emailUser = $('#salesemail').val();
        var phoneNumberUser = $('#salesphone').val();
        var userInput = $('#salestextarea').val();
        //check for valid inputs  
        if (validEmail(emailUser) && validPhoneNumber(phoneNumberUser) && validName(userInput)
            && validUserName(userName) && validOrganizationName(organizationName) && validRole(role)) {
            //     var form = document.getElementById('gform');
            //     form.addEventListener("submit", handleFormSubmit, false);
            handleFormSubmit(event, "gform");
        }
        else {
            alert('Enter all valid details')
            //prevent form posting           
            event.preventDefault();
            //console.log('invlaid')
        }
    })

    $('.technicalsupport-btn').click(function (event) {
        debugger
        var userName = $("#tsname").val();
        var organizationName = $("#tsorganizationname").val();
        var role = $("#tsrole").val();
        var weburl = $("#tswebsiteurl").val();
        var emailUser = $('#tsemail').val();
        var phoneNumberUser = $('#tsphone').val();
        var userInput = $('#tstextarea').val();
        //check for valid inputs  
        if (validEmail(emailUser) && validPhoneNumber(phoneNumberUser) && validName(userInput)
            && validUserName(userName) && validOrganizationName(organizationName) && validRole(role)) {
            //     var form = document.getElementById('gform');
            //     form.addEventListener("submit", handleFormSubmit, false);
            handleFormSubmit(event, "tform");
        }
        else {
            alert('Enter all valid details')
            //prevent form posting           
            event.preventDefault();
            //console.log('invlaid')
        }
    })

})
//Email validation
function validEmail(email) { // see:
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
//Phone Validation
function validPhoneNumber(phoneno) { // see:
    var re = /^\+?([6-9]{1})([0-9]{9})$/;
    return re.test(phoneno);
}
//Name length validaions
function validName(name) { // see:
    var re = /^[a-zA-Z ]+$/;
    return re.test(name);
}
//Name userName
function validUserName(name) {
    var re = /^[a-zA-Z ]+$/;
    return re.test(name);
}

//Name organizationName
function validOrganizationName(organizationName) {
    var re = /^[a-zA-Z ]+$/;
    return re.test(organizationName);
}

//Name role
function validRole(role) {
    var re = /^[a-zA-Z ]+$/;
    return re.test(role);
}

//Name website url
function validWebUrl(weburl) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(weburl);
}




//  Setting up the map in contact us page
function myMap() {
    var myLatLng = {
        lat: 12.977716,
        lng: 77.725187
    };

    var mapOptions = {
        center: myLatLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'PalC Networks Pvt Ltd'
    });

}

function getFormData(id) {

    var elements = document.getElementById(id).elements; // all form elements
    var fields = Object.keys(elements).map(function (k) {
        if (elements[k].name !== undefined) {
            return elements[k].name;
            // special case for Edge's html collection
        } else if (elements[k].length > 0) {
            return elements[k].item(0).name;
        }
    }).filter(function (item, pos, self) {
        return self.indexOf(item) == pos && item;
    });
    var data = {};
    fields.forEach(function (k) {
        data[k] = elements[k].value;
        var str = ""; // declare empty string outside of loop to allow
        // it to be appended to for each item in the loop
        if (elements[k].type === "checkbox") { // special case for Edge's html collection
            str = str + elements[k].checked + ", "; // take the string and append 
            // the current checked value to 
            // the end of it, along with 
            // a comma and a space
            data[k] = str.slice(0, -2); // remove the last comma and space 
            // from the  string to make the output 
            // prettier in the spreadsheet
        } else if (elements[k].length) {
            for (var i = 0; i < elements[k].length; i++) {
                if (elements[k].item(i).checked) {
                    str = str + elements[k].item(i).value + ", "; // same as above
                    data[k] = str.slice(0, -2);
                }
            }
        }
    });
    data["page"] = document.getElementById(id).getAttribute("name");
    //console.log(data);
    return data;
}

function handleFormSubmit(event, id) {
    // handles form submit withtout any jquery
    event.preventDefault();           // we are submitting via xhr below
    var data = getFormData(id);         // get the values submitted in the form
    if (validEmail(data["email"]) && validName(data["enquiry"]) && validPhoneNumber(data["mobileNumber"])) {

        // var url="https://script.google.com/macros/s/AKfycbxvRnVCB-7H14XYiCd0iUlkodaW04ZUP9MXnSNbdwOCVVGSyc4/exec";
        //var url = "https://script.google.com/macros/s/AKfycbxvRnVCB-7H14XYiCd0iUlkodaW04ZUP9MXnSNbdwOCVVGSyc4/exec";
        var url = "https://script.google.com/macros/s/AKfycbxV8C7gCRtHg4O8vE_IG7y3sX-0AC0FMUzEjFQekYgESKtnmT4j/exec";
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        // xhr.withCredentials = true;
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onreadystatechange = function () {
            // console.log(xhr);
            //console.log(xhr.status, xhr.statusText)
            //console.log(xhr.responseText);
            // document.getElementById('gform').style.display = 'none'; // hide form
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    $("[type=text]").val("");
                    $("[type=email]").val("");
                    $("[type=number]").val("");
                    alert("Your form has been successfully submitted.");
                    if (data.page === "Technical Support") {
                        $('#technicalSupportModal').modal('toggle');
                    }
                    if (data.page === "Sales Inquiry") {
                        $('#salesInquiryModal').modal('toggle');
                    }
                    if (data.page === "Contact Us") {
                        $('#contactusModal').modal('toggle');
                    }
                    window.location.reload();
                } else {
                    alert("Please submit the form again.");
                }
            }
            return;
        }
    } else {
        alert("Please check the form again!!");
    }
    // url encode form data for sending as post data
    var encoded = Object.keys(data).map(function (k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
    }).join('&')
    xhr.send(encoded);
}
$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {
        $('#return-to-top').fadeIn(200);
    } else {
        $('#return-to-top').fadeOut(200);
    }
});
$('#return-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 500);
});
$(document).ready(function () {
    $('.footerNetwork').click(function () {
        localStorage.setItem('activeTab', "#serviceNav-home")
    })
    $('.footerCloud').click(function () {
        localStorage.setItem('activeTab', "#serviceNav-profile")
    })
    $('.footerDataAnalytics').click(function () {
        localStorage.setItem('activeTab', "#serviceGoestodataanalytics")
    })
    $('.footerDevops').click(function () {
        localStorage.setItem('activeTab', "#serviceGoestoEnterpriseApplication")
    })
});

$(document).ready(function () {
    $(".navServiceText").click(function (e) {
        e.stopPropagation();
    });
    $('.dropdown-item').on('click', function (e) {
        var $el = $(this).children('.dropdown-toggle');
        var $parent = $el.offsetParent(".dropdown-menu");
        $(this).parent("li").toggleClass('open');

        if (!$parent.parent().hasClass('navbar-nav')) {
            if ($parent.hasClass('show')) {
                $parent.removeClass('show');
                $el.next().removeClass('show');
                $el.next().css({ "top": -999, "left": -999 });
            } else {
                $parent.parent().find('.show').removeClass('show');
                $parent.addClass('show');
                $el.next().addClass('show');
                $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 1 });
            }
            e.preventDefault();
            e.stopPropagation();
        }
    });
    $('.navbar .dropdown').on('hidden.bs.dropdown', function () {
        $(this).find('li.dropdown').removeClass('show open');
        $(this).find('ul.dropdown-menu').removeClass('show open');
    });
});
